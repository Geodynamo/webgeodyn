FROM ubuntu:22.04
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update  && apt-get install -y -qq \
    python3 \
    python3-pip
RUN apt-get autoclean -y && apt-get autoremove -y && rm -rf /var/lib/apt/lists/*
RUN pip3 install coverage
RUN pip3 install sphinx
RUN pip3 install twine