#########
webgeodyn
#########

.. image:: https://gricad-gitlab.univ-grenoble-alpes.fr/Geodynamo/webgeodyn/badges/master/build.svg
    :target: https://gricad-gitlab.univ-grenoble-alpes.fr/Geodynamo/webgeodyn/pipelines

.. image:: https://img.shields.io/website/https/geodynamo.gricad-pages.univ-grenoble-alpes.fr/webgeodyn/index.html.svg?label=documentation&up_color=cyan
    :target: https://geodynamo.gricad-pages.univ-grenoble-alpes.fr/webgeodyn/index.html
.. image:: https://img.shields.io/pypi/v/webgeodyn.svg
    :target: https://pypi.org/project/webgeodyn/

**webgeodyn** is a web-based plot tool to visualize Earth core flows and scalar fields at the Core Mantle Boundary (CMB). It consists in a Tornado web server, that can be started locally, that provides a set of visualisation tools on a variety of data formats.

Installation
============
Requirements
------------
The installation of webgeodyn requires Python 3 to be installed.

The other dependencies will be automatically installed by the next step but are listed here for the sake of completeness:

* *numpy*
* *scipy* (version > 1.1)
* *h5py*
* *tornado*
* *cdflib*
* *astropy*

Installing the package
----------------------
webgeodyn can be installed

- from pip:

.. code-block:: bash

    pip3 install webgeodyn [--user]

Put the :code:`--user` flag if you are not installing in a `virtual environment <https://docs.python.org/3/library/venv.html>`_.

- from the git repository :

Clone first the webgeodyn repository

.. code-block:: bash

    git clone https://gricad-gitlab.univ-grenoble-alpes.fr/Geodynamo/webgeodyn.git

Then install the package:

.. code-block:: bash

    cd webgeodyn
    python3 setup.py install [--user]

Again, put the :code:`--user` flag if needed.

Whatever the method used, you can test if the install succeed by importing webgeodyn in Python3:

.. code-block:: bash

    python3 -c "import webgeodyn; print(webgeodyn.__version__)"

This command should return the installed version.

Running the example
===================

You can give a first try at starting the web server by running the example:

.. code-block:: bash

    python3 webgeodyn/example.py

or in the Python console:

.. code-block:: python

    >>> import webgeodyn.example
    >>> webgeodyn.example.run()

This starts the server locally and should open your browser and display a page resembling the one at https://geodyn.univ-grenoble-alpes.fr/. If not, try to type :code:`http://localhost:8080` in your browser.

You can try the different visualisations tools provided on the loaded example model (`CHAOS-7 <http://www.spacecenter.dk/files/magnetic-models/CHAOS-7/>`_).

Note that this example will also try to load the result from the latest `pygeodyn <https://gricad-gitlab.univ-grenoble-alpes.fr/Geodynamo/pygeodyn>`_ (geomagnetic data assimilation Python package also developed in our group) computation (if present in ``~/pygeodyn_results/Current_computation/``).

Running the server with your data
=================================
The server can be used to visualise any data of supported format. For that, it is necessary to follow the template of ``example.py``:

- First, load the data under the form of `Model`_ objects, of a given name and format, in a `Models`_ dictionary.
- Then, the server must be started with the loaded `Models`_.

.. _Model: https://geodynamo.gricad-pages.univ-grenoble-alpes.fr/webgeodyn/webgeodyn.models.model.html#webgeodyn.models.model.Model
.. _Models: https://geodynamo.gricad-pages.univ-grenoble-alpes.fr/webgeodyn/webgeodyn.models.models.html

This is shown in details below:

.. code-block:: python

    # 0.Import the necessary submodules
    import webgeodyn.server
    import webgeodyn.models

    # 1.Initialising the Models dictionary
    models = webgeodyn.models.Models()

    # 2.Loading your data in the Models dictionary
    # Syntax: models.loadModel('/path/to/the/model/directory', "Name of the model", "Format of the model")
    # Example for pygeodyn:
    models.loadModel('pygeodyn_results/Current_computation', 'Current pygeodyn computation', 'pygeodyn_hdf5')
    # Several models can be loaded at once. Example for CHAOS:
    models.loadModel('webgeodyn/webgeodyn/example_data/CHAOS-7', 'CHAOS-7.13', 'CHAOS')

    # 3.Start the server with the loaded Models
    webgeodyn.server.startServer(models,{})

By copying this code in a Python file of your own, you should be able to use the visualisation tools on data of supported formats.

The format of the models, that define the format of the files to read, are the modules of `webgeodyn.inout`_. Here are some dataFormat examples:

- ``archomag``: to read COVARCH et COVLAKE files
- ``chaos``: to read `CHAOS`_ splines files
- ``covobs``: to read  `COVOBS`_ realisations files in the spherical harmonics basis
- ``covobs_splines``: to read `COVOBS`_ realisations files filled with splines coefficients
- ``enscore``: to read files generated using [GBF15]_-
- ``pygeodyn_asc``: for files in the old ASCII format used in pygeodyn
- ``pygeodyn_hdf5``: to read HDF5 files generated by `pygeodyn`_
- ``ZForecast``: to read files generated by [BHF18]_ or [BGA17]_

A list of the formats can be displayed by running:

.. code-block:: python

    >>> import webgeodyn.inout
    >>> print(webgeodyn.inout._formats)

If you need the support of a new format of file, you can follow the templates given in the documentation of `webgeodyn.inout`_ to implement your own loading function. Otherwise, you can contact us using the information given below.

.. _COVOBS: http://www.space.dtu.dk/english/Research/Scientific_data_and_models/Magnetic_Field_Models
.. _CHAOS: http://www.space.dtu.dk/english/Research/Scientific_data_and_models/Magnetic_Field_Models
.. _pygeodyn: https://gricad-gitlab.univ-grenoble-alpes.fr/Geodynamo/pygeodyn
.. _webgeodyn.inout: https://geodynamo.gricad-pages.univ-grenoble-alpes.fr/webgeodyn/index.html/webgeodyn.inout.html

Developer documentation
=======================
Documentation of the submodules of the package are available `online <https://geodynamo.gricad-pages.univ-grenoble-alpes.fr/webgeodyn/index.html>`_.

If `Sphinx <http://www.sphinx-doc.org/>`_ is installed and the files were cloned from the repository, it is possible to generate the documentation locally using:

.. code-block:: bash

    cd doc && ./make_all_doc.sh

The documentation will then be available in HTML format at doc/html/index.html.

Conditions of use
=================
The work is licensed under the `GNU GPLv3 <./LICENSE.txt>`_.

Git repository
==============
The source code is stored on a Git repository (https://gricad-gitlab.univ-grenoble-alpes.fr/Geodynamo/webgeodyn) which can also be used to give feedbacks through `Issues <https://gricad-gitlab.univ-grenoble-alpes.fr/Geodynamo/webgeodyn/issues>`_.

References
==========
.. [GBF15] Gillet, N., Barrois, O. & Finlay, C. C. Stochastic forecasting of the geomagnetic field from the COV-OBS.x1 geomagnetic field model, and candidate models for IGRF-12. *Earth, Planets and Space* 67, (2015). doi:10.1186/s40623-015-0225-z
.. [BGA17] Barrois, O., N. Gillet, and J. Aubert. "Contributions to the geomagnetic secular variation from a reanalysis of core surface dynamics." Geophysical Journal International 211.1 (2017): 50-68.
.. [BHF18] Barrois, O., Hammer, M. D., Finlay, C. C., Martin, Y. & Gillet, N. Assimilation of ground and satellite magnetic measurements: inference of core surface magnetic and velocity field changes. *Geophysical Journal International* (2018). doi:10.1093/gji/ggy297

Contact information
===================
For scientific inquiries, contact `Nicolas Gillet <mailto:nicolas.gillet@univ-grenoble-alpes.fr>`_. For technical problems, contact `Francois Dallasta <francois.dall-asta@univ-grenoble-alpes.fr>`_ and/or `Franck Thollard <mailto:franck.thollard@univ-grenoble-alpes.fr>`_.
