######################
Changelog of webgeodyn
######################

0.10.5 - 2023-12-18
-------------------
* Updated GOVO data 12M

0.10.4 - 2023-07-26
-------------------
* Added Shear ('S') at CMB to measure list.

0.10.3 - 2023-04-28
-------------------
* Increased title and legend font size in "Earth's core surface visualization"
* Removed Divhu and SA from "Spherical harmonics coefficients visualization" and "Lowes-Mauersberger spectra"
* Updated GVO data in "Virtual or geomagnetic observatories visualization" and adjusted display

0.10.2 - 2019-12-13
-------------------
* Updated GO and VO data to V31 and 0121 versions
* covobs_splines method no longer fails silently when no files are found
* CSS files are now generated from SCSS files

0.10.1 - 2019-11-26
-------------------
* pygeodyn_hdf5_forecasts loading method now works for all pygeodyn versions
* ModelName can be omitted when loading Models (uses the basename of the directory).

0.10.0 - 2019-10-29
-------------------
* The computation of Lowes spectra is now much faster
* Improvements of the handling of RMS in the spherical harmonic tab
    - The display mode can be changed from shaded area to RMS
    - The tooltip of series with RMS now shows the low and high values

0.9.2 - 2019-10-23
------------------
* COVOBS splines models now also load the SA and handle correctly realisations.

0.9.1 - 2019-10-22
------------------
* COVOBS splines loading method is now much faster (reimplemented with scipy.interpolate.Bspline).

0.9.0 - 2019-10-04
------------------
**New features:**

- Implemented a dedicated tab for the plot of the length-of-day (LOD)
- Implemented external field (EF) in the spherical harmonic coefficients' plot

**Fixes:**

- Importing `webgeodyn.example` no longer starts the server
- Several improvements of HTML and CSS

0.8.1 - 2019-08-09
------------------
* Clean-up in docs:
    - Module READMEs are now in docstrings to be displayed with Sphinx
    - CHANGELOG and README were converted to *rst* and are now integrated in the Sphinx doc
* ``pygeodyn_hdf5_forecasts`` format is now compatible with pygeodyn >= 1.1

0.8.0 - 2019-08-07
------------------
* COVOBS splines loading method now returns the SV
    - This is done by implementing the expression of derivative splines that is tested against the numerical counterpart
    - The test fails for scipy<1.1 so webgeodyn now requires scipy>1.1
* New versions of webgeodyn should now normally be uploaded automatically on PyPI

0.7.1 - 2019-06-07
------------------
* Fixed COVOBS splines loading method: 
    - The spline order read in the file must be shifted: it indeed starts at 1 in COVOBS whereas the one in scipy starts at 0. 
    - Updated the test in consequence with an absolute tolerance of 10e-4

0.7.0 - 2019-06-03
------------------
**New features:**

- Updated GO and VO data to latest version (2019-04)
- Implemented the support of realisations for `covobs_splines` format

0.6.0 - 2019-04-18
------------------
**New features:**

- Includes new features of EarthCoreVisu 1.1.0
    - Added stereographic projection
    - Added a slider to tune to coastline widths
- Added CHANGELOG
- Added utilities for a banner and the homepage

**Fixes:**

- Removed unnecessary parts (pid, exportTab)
- Fixed overflowing main div in homepage

0.5.0 - 2019-04-01
------------------
Release of webgeodyn, a web-based tool to visualise geomagnetic data that can be deployed locally.

* Package available on `PyPI <https://pypi.org/project/webgeodyn/>`_.