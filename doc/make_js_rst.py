import os
import glob


def make_rst_title(rst_file, rst_name, rst_type="class"):
    """
    Write the title of the rst file using the rst_name and type.

    :param rst_file: File handler to write
    :type rst_file: IO
    :param rst_name: name of the rst
    :type rst_name: str
    :param rst_type: Type of the rst ("class" or "package")
    :type rst_type: str
    """
    full_title = rst_name+" "+rst_type.lower()
    rst_file.write(full_title+"\n")
    rst_file.write("="*len(full_title) + "\n")
    rst_file.write("\n")


# Define name of the package (first level folder)
package_name = 'webgeodyn.www'
# Define name of the module relative to the package (second level folder)
module_names = ['controller', 'model', 'ui']

# Add the JS package to the packages of webgeodyn
if not os.path.isfile('webgeodyn.rst'):
    raise IOError('No webgeodyn.rst file was found !')

f = open('webgeodyn.rst', 'r')
contents = f.readlines()
f.close()

# Insert the package name after the blank line following toctree
for i, line in enumerate(contents):
    if "toctree" in line:
        contents.insert(i+2, '    '+package_name+'\n')
        break

# Overwrite webgeodyn.rst with the new content
f = open('webgeodyn.rst', 'w')
contents_strings = "".join(contents)
f.write(contents_strings)
f.close()

# Create the package rst
with open(package_name+'.rst', 'w') as rst_file:
    make_rst_title(rst_file, package_name, 'package')
    rst_file.write('Subpackages\n'
                   '-----------\n'
                   '\n'
                   '.. toctree::\n'
                   '\n')
    for f in module_names:
        rst_file.write('    '+package_name+'.'+f+'\n')

    rst_file.write('\n'
                   'Module contents\n'
                   '---------------\n'
                   '\n'
                   '.. automodule:: '+package_name+'\n'
                   '    :members:\n')
    rst_file.close()

# Deal now with each module
for m in module_names:
    # Get the submodules files using glob
    rst_module_submodules = [(file[3:].split('.')[0]).replace('/', '.') for file in glob.glob('../'+package_name.replace('.', '/')+'/'+m+'/*.js')]

    # Create the module rst
    rst_module_name = package_name+'.'+m
    with open(rst_module_name+'.rst', 'w') as rst_file:
        make_rst_title(rst_file, rst_module_name, 'package')
        # Put all found submodules in the toctree
        rst_file.write('Submodules\n'
                       '----------\n'
                       '\n'
                       '.. toctree::\n'
                       '\n')
        for f in rst_module_submodules:
            rst_file.write('    '+f+'\n')

        rst_file.write('\n'
                       'Module contents\n'
                       '---------------\n'
                       '\n'
                       '.. automodule:: '+rst_module_name+'\n'
                       '    :members:\n')
        rst_file.close()

    # Create a rst for each submodule.
    # The submodule should contain only a class with the same name as the file but with a capitalised letter.
    for rst_name in rst_module_submodules:
        with open(rst_name+'.rst', 'w') as rst_file:
            make_rst_title(rst_file, rst_name, 'class')
            basename = rst_name.split('.')[-1]
            # Capitalise only the first letter
            rst_file.write('.. js:autoclass:: '+basename[0].upper()+basename[1:]+'\n')
            rst_file.write('    :members:')
            rst_file.close()
