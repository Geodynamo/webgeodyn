#!/usr/bin/env bash
# Clean up existing rst files
rm -rf webgeodyn*.rst
# Generate Python rst using sphinx tool
sphinx-apidoc -e -o . -f ../webgeodyn
# Generate JS rst using the python script
#python3 make_js_rst.py
# Copy CHANGELOG and README
cp ../CHANGELOG.rst ../README.rst .
# Build the sphinx pages in html
sphinx-build -a -b html . ./html
